package QueuePack;

public class QueueMain {

	public static void main(String[] args) {
		QueueEx1  q = new QueueEx1(5);
		q.enQueue(10);
		q.enQueue(20);
		q.enQueue(30);
		q.enQueue(40);
		q.enQueue(50);
		
		System.out.println("Queue Values ");
		q.ShowQueueValues();
		
		int n = q.Size();
		System.out.println();
		for(int i=0;i<n;i++)
		{
			System.out.println("Removing : " +  q.deQueue());
		}
		
		if(q.isEmpty())
			System.out.println("No values Present in Queue");
	}

}
