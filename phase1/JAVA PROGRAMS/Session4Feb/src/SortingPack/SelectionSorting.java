package SortingPack;

public class SelectionSorting {

	public static void main(String[] args) {
		
		int x[] = {10,3,4,1,5};
		
		System.out.println("Actual Values ");
		for(int i=0;i<x.length;i++)
		{
			System.out.print(x[i] + "  ");
		}
		
		
		for(int i=0;i<x.length;i++)
		{
			int minindex = i;
			for(int j=i+1;j<x.length;j++)
			{
				if(x[j]<x[minindex])
				{
					minindex = j;
				}
			}
			int temp = x[i];
			x[i] = x[minindex];
			x[minindex] = temp;
		}
				
		System.out.println("\nSorted Values ");
		for(int i=0;i<x.length;i++)
		{
			System.out.print(x[i] + "  ");
		}

	}

}
