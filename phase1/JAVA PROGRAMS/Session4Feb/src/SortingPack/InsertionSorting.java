package SortingPack;

public class InsertionSorting {

	public static void main(String[] args) {
		
		int x[] = {10,3,4,1,5};
		
		System.out.println("Actual Values ");
		for(int i=0;i<x.length;i++)
		{
			System.out.print(x[i] + "  ");
		}
		
		
		for(int i=1;i<x.length;i++)
		{
			int temp = x[i];
			int j=i-1;
			while(j>=0 && x[j]>temp)
			{
				x[j+1] = x[j];
				j--;
			}
			
			x[j+1] = temp;
		}
		
		System.out.println("\nSorted Values ");
		for(int i=0;i<x.length;i++)
		{
			System.out.print(x[i] + "  ");
		}

	}

}
