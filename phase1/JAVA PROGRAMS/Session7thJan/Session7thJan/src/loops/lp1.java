package loops;

public class lp1 {

	public static void main(String[] args) {

		int x = 1;
		while(x<=5)
		{
			System.out.println(x + " Java Training...");
			x++;
		}
		
		System.out.println("-----------");
		
		
		int y = 5;
		while(y>=1)
		{
			System.out.println(y + " Java Training");
			y--;
		}

	}

}
