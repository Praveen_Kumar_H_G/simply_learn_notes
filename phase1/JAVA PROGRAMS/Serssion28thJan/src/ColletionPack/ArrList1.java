package ColletionPack;
import java.util.*;
public class ArrList1 {

	public static void main(String[] args) {
		List<String> arrList = new ArrayList<String>();
		arrList.add("Pushpa");
		arrList.add("Yogendra");
		arrList.add("Venugopal");
		arrList.add("Krishna");
		arrList.add("Pushpa");
		
		System.out.println(arrList);
		// traversing  using foreach loop
		for(String str : arrList) // foreach
		{
			System.out.println(str);
		}
		System.out.println("-----------------");
		//using Iterator interface
		Iterator itr = arrList.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
	}

}
