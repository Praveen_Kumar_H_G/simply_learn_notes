package com.cons;

public class MultiCons {

	private int x, y;
	
	public MultiCons()
	{
		System.out.println("It is a constructor without parameters");
		x = 10;
	    y = 20;
	}
	
	public MultiCons(int x, int y)
	{
		System.out.println("It is a constructor with parameters");
		this.x = x;
		this.y = y;
	}
	
	public void PrintValues()
	{
		System.out.println("X value : " + x);
		System.out.println("Y value : " + y);
	}
}
