package com.apps;
import com.cons.DemoGetterAndSetter;
public class GettersAndSettersApp {

	public static void main(String[] args) {
		
		DemoGetterAndSetter d1 = new DemoGetterAndSetter();
		d1.setX(10);
		System.out.println("Given Value : " + d1.getX());
	}

}
