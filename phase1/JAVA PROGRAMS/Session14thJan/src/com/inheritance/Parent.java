package com.inheritance;

public class Parent {

	protected int x, y;
	
	public void GetData(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}

class Child extends Parent
{
	private int sum;
	
	public void PutData()
	{
		sum = x+y;
		System.out.println("X val : " + x);
		System.out.println("Y val : " + y);
		System.out.println("Sum val : " + sum);
	}
}
