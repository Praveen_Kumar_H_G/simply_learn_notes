package com.exceptions;
import java.util.*;
public class Ex7 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try
		{
			System.out.println("Enter age of the person");
			int age = sc.nextInt();
			
			if(age<18)
				throw new InvalidAgeException("Person age is below 18 years");
			else
				System.out.println("Voter Details are Registerd");
		}
		catch(InvalidAgeException  ag)
		{
			System.out.println(ag.getMessage());
		}
		

	}

}
