package TestPackage;

public class PrgExample2 {

	public static void main(String[] args) {
		byte x = 100;
		System.out.println(x);
		short y = 30000;
		System.out.println(y);
		int z = 450000;
		System.out.println(z);
		
		char ch = 'A';
		System.out.println(ch);
		
		float f1 = 12.45456789f;
		System.out.println(f1);
		
		double f2 = 456.6781234567890123456;
		System.out.println(f2);
		
		boolean b = false;
		System.out.println(b);
	}

}
