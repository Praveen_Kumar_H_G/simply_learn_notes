package TestPackage;
// Example for Relational Operators
public class PrgExample3 {

	public static void main(String[] args) {
		int x = 10, y = 3;
		System.out.println("X value is : " + x);
		System.out.println("Y value is : " + y);
		
		System.out.println(x + " > " + y + " = " +(x>y));
		System.out.println(x + " < " + y + " = " +(x<y));
		System.out.println(y + " < " + x + " = " +(y<x));
		System.out.println(x + " != " + y + " = " +(x!=y));
		System.out.println(x + " == " + y + " = " +(x==y));
	}

}
