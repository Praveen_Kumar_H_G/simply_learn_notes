package com.ds;

public class BubbleSorting {

	public static void main(String[] args) {
		int x[] = {10,3,4,1,5};
		System.out.println("Actual Values ");
		for(int i=0;i<x.length;i++)
		{
			System.out.print(x[i] + "  ");
		}
		
		System.out.println("\nBubble Sorting ");
	
		for(int i=0;i<x.length;i++)
		{
			for(int j=i+1;j<x.length;j++)
			{
				if(x[i]>x[j])
				{
					int temp = x[i];
					x[i] = x[j];
					x[j] = temp;
				}
			}
		}

		System.out.println("Sorted Values ");
		for(int i=0;i<x.length;i++)
		{
			System.out.print(x[i] + "  ");
		}
		
	}

}
