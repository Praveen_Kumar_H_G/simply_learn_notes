package com.ds;

public class CircularMain {

	public static void main(String[] args) {
		CircularLinkedList  clist = new CircularLinkedList();
		
		clist.addNode(100);
		clist.addNode(200);
		clist.addNode(300);
		clist.addNode(400);
		clist.addNode(500);
		
		clist.ShowValues();
	}

}
