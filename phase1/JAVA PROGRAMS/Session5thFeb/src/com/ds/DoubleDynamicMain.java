package com.ds;

import java.util.Scanner;

public class DoubleDynamicMain {

	public static void main(String[] args) {
		DoubleLinkedList  dlist  = new DoubleLinkedList();
		
		Scanner sc = new Scanner(System.in);
		
		while(true)
		{
			System.out.println("Enter any value (-99 to stop ) :");
			int val = sc.nextInt();
	
			if(val==-99)
				break;
	
			dlist.addNode(val);		
		}
		
		
		dlist.ShowValuesWithBackward();
		
		System.out.println("\n----------------");
		dlist.ShowValuesWithForward();

	}

}
