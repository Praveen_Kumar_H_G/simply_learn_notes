package com.ds;

public class DoubleMain {

	public static void main(String[] args) {
		DoubleLinkedList  dlist  = new DoubleLinkedList();
		dlist.addNode(100);
		dlist.addNode(200);
		dlist.addNode(300);
		dlist.addNode(400);
		dlist.addNode(500);
		
		dlist.ShowValuesWithBackward();
		
		System.out.println("\n----------------");
		dlist.ShowValuesWithForward();

	}

}
