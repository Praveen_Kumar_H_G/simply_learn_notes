Today's Agenda:-
----------------
1. throw, throws
2. I/O Streams
3. Multi Threading
4. Sync
-----------------

Classification of Exceptions:- 
------------------------------
Exceptions are classified into 3 types

1.  Unchecked Exceptions
		these exceptions are identified at the time of progrm execution and handled with 	
			try and catch blocks. 
2.  Checked Exceptions
		these exceptions are identified at the time of coding the program or compling the program. 
		To handle these exceptions "throws" has to be used. 

	void methodname(....)  throws ExceptionName
	{
		body of the method
	}

read() :-  it is used to read a character from keyborad and returns it's ascii  value. 
		it will raise an exception and that has to be handled with "throws".
variable = System.in.read()  


3.  User Defined Exceptions
		the exceptions created and handled by the user as per the requirement is called as 
		user defined exceptions

Steps:- 

1 Create a class as follows

class <classname> extends Exception
{
	classname(arguments)
	{
		super(argVal);
	}
}

2.  To raise execption using "throw" keyword as follows  in try block and handle in catch block.
	throw  new exceptionclassname(argval);


situations of User Defined Exceptions:-
-------------------------------------
I am Creating VoterList Program, where  if voter age below 18 program execution has to stop. 

	InvalidAgeException
----
I am Creating A banking program, where if account is not having sufficient balance for load emi, then program execution has to stop. 

	InsufficientFundException
-------------------------------------------------------------------------------------------------
Input / Output Streams:- 

Stream :- it is the process of flowing data from one medium to another medium. 
				or
	it is the process of flowing sequence of bits and bytes from one medium to another medium.

There are two types of Streams

1. Input  Stream :-
	
InputStream class
InputStream class is an abstract class. It is the superclass of all classes representing an input stream of bytes.

Useful methods of InputStream
Method	Description
1) public abstract int read() throws IOException
	reads the next byte of data from the input stream. It returns -1 at the end of the file.
2) public int available() throws IOException	
	returns an estimate of the number of bytes that can be read from the current input stream.
3) public void close() throws IOException	is used to close the current input stream.


2. Output Stream :- 

OutputStream class
OutputStream class is an abstract class. It is the superclass of all classes representing an output stream of bytes. 
An output stream accepts output bytes and sends them to some sink.

Useful methods of OutputStream
Method	Description
1) public void write(int)throws IOException	is used to write a byte to the current output stream.
2) public void write(byte[])throws IOException	is used to write an array of byte to the current output stream.
3) public void flush()throws IOException	flushes the current output stream.
4) public void close()throws IOException	is used to close the current output stream.
------
Files Concept:-  this concept is used to write data into file and read data from file programatically. 

FileOutputStream  & FileInputStream :- 


FileOutputStream:- this class allows to create a file and stores address in an objects. 
FileInputStream :- this class allows to read a char from file. 

FileWriter :- this class allows to write data into file in the form of characters directly.
		it gives another feature, like to append data to file if file already exist.
			FileWriter  <obj> =new FileWriter("filename", true/false)
				true :- append data
				false :-  only write data.
FileReader :- this class allows to read data from file line by line. 

FileReader  <object>  new FileReader("filename");

BufferedReader  <object> = new BufferedReader(Object of Filereader);

readLine() :-  this method is used to read text line by line. 


File :- this class is used to perform different operations with the help of some methods. 

File  <object> = new File("Filename/FolderName/Location);

createNewFile() :-  it allows to create new empty file. 
mkdir() :- to create a folder
exists() :- it checks weather file / folder existed or not. 
isFile() :-  it checks given name is file or not.
isDirectory() :- it checks given name is folder or not. 
delete() :- it allows to delete a file / folder. 
list() :- it allows shows all the files and folders from specified location. 
-------------------------------------------------------------------------------------------------
Multi Threading :- 
-----------------
Multi Threading :- 
-----------------------
Thread :-  It is a process/task. 

Multi Threading :-  it is the process of executing multiple tasks at a time. 

Except  Multi Threding  rest of all concepts comes under Single Threaded Programming. 
i.e.  tasks are executed one after another only. 


class  Animation
{

	void waterwaves()
	{
		.....
	}

	void birdsfly()
	{
		.......
	}
}

class a
{
	void main()
	{
		Animation ani;
		ani.waterwaves();
		ani.birdsfly();
	}
}
// Single Thread Programming


Need of Multi Threading :-
--------------------------
1.  Where we need to execute multiple task at a time(Mostly GUI Programming).
2.  Faster Execution, In  multi Threading apporach, every thread is a light weight unit, so that execution will be faster. 
3.  Threads are independent, so that at any case if one thread gets an execption, it won't reflected to other thread. 

Multi Threading  and Multi Tasking

Operating Systems:-  GUI Based OS are multi tasking based OS's.

Is the processor can process multiple tasks at time?

No, Based on Time Sharing, every task will be processed.

Multi Threading programming can be achieved in following two ways

1.  With the help of "Thread" class
2.  With the help of "Runnable" Interface

Thread Class :-  This class has to be extended to the user defined class
		it has following methods
			run() :-  it is an abstract method where it allows to define task of the 
					threading. 

			start() :-  it will start the executon of threading, i.e. actually
					it calls run() method for an execution. 

			sleep() :-  this method suspends a thread execution for specific interval
					period of time. 
-------------------------------------
Method-1 :-  By extending Thread class
-------------------------------------

1.  Create a class then extend Thread class
2.  Define a task in run() method
3.  Create an object for User Defined class, then use Start() method to execute thread. 

Method-2 :-  By implementing Runnable Interface
-----------------------------------------------
1.  Create a class then implement runnable interface.
	Note :-  Runnable interface is having only run() method declaration.
2.  Define run() method with a task. 
3.  Create an object for user defined class. 
4.  Create an object for Thread class, while creating object, we need pass user defined class object 
	as constructor parameter. 
5.  Now execute thread with start() method.
----------------------------
Thread Life Cycle:-  As per threading there are 5 stages to complete execution process of the thread.

1.  New Born State
2.  Runnable state
3.  Running State
4.  Blocked State
5.  Dead State
--------------------------------------------
Naming a Thread :-  
		whenever we create any thread, that will have a default name, those names starts
		thread-0, thread-1, ......, if we want we can change those names. 

		getName() :-  to show thread name.
		setName(String) :-  to set new name for a thread

currentThread():- this method returns currently running thread.
----------------------------------
Priority of the thread :-  With the help of thread priority, we can tell to the process order of thread execution.

As per java, thread prioritites are b/w 1-10. by default every thread having proiority.

MAX Priority = 10  :- it will execute first
Min Priority = 1   :- it will execute last 
Default Priority = 5 :- 

getPriority() :-  to show priority
setPriority(value) :-  to set order of priority.






