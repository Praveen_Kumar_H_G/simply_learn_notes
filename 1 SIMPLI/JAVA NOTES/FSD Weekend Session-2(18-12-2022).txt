What are the keywords of Integer Type. 

byte, short,  int, long

What is a Byte Code?
	
What is a package

What is the extension of Byte code file.

What is a String?

What is a character?

What is variable?

Diff b/w constant and variable?
------------------------------------------------------
Today's Agenda:-

1.  Basic Programs
2.  Variations of Different Data Types
3.  Operators
4.  Type Casting 
5.  Scanner Class
6.  Control Statements 
		If statement
		switch statement
		looping statements(while, for, do-while)
-------------------------------------------------------
Characterstics of each data type:- 

	Once a variable is created with specific data type?
		it has following things
			- variable value
			- variable name
			- variable value range
			- variable size(how much space occupies in memory)

	Note:-  As per java variables can be declared any where in the program(method)
		before it's usage.

--------------------------------------------------------------------
Operators :- 
these are the symbols which is having an action b/w operands. 

Arithematic :- 
		+, -, /, *, %
		/  - division - Quoficient
		%  - division - Remainder
Relational :- to check conditions
		>, <, >=, <=, !=(not equals), ==(equals)

Logical :-  used to check multiple conditions. i.e. in b/w two conditions any one operator has to used
		&& (AND)  =>  true :  when all conditions are true
		||  (OR)  =>  true :  when any one condition is true
		!  (Not)  =>  true :  when given condition is false

Unary Operators :- 
			++ = increment by 1
			-- = decrement by 1
	these are classified into two types
		Pre increment / decrement
				operator placed left side  of the variable
				++x
				int x = 10
				Here First Action then assignment will be done.
				k = ++x;   // k = 11, x = 11

		Post increment / decrement
				operator placed right side of the variable
				x++
				Here First Assignment then action will be done.
				k = x++;  // k = 10, x = 11
Assignemnt Operators :-  these are used to assign values / expression result to the variables.
				=  (to assgin)
				+=, -=, /=, *=, %=

Conditional :-  these are used to check conditions. (?  :)
			<condition> ? <true> : <false>

Bitwise :-   
		<< left shift 
		>> right shift
		& 
		|

Concatenation Operator :- it is used merge any two strings or string with other type value. 
				the operator is "+".
--------------------------------------------------------------------------
Type Casting :- 

The process of converting values from one data type to another data type. 

there are two types

1.  Implicit
		this process will from lowest data type to highest data type.

			byte-short-int-long-float-double 
2.  Explicit
		this process will from highest data type to lowest data type.

			double-float-long-int-short-byte
----------------------------------------------------------------------------
Scanner Class :- this class is used to give input to the variable dynamically by reading data from 
			input stream(keyboard/mouse/.....).

1.  import java.util package. 
		import java.util.Scanner;
2.  Create an object for Scanner Class as follows
		Scanner  <objectname> = new Scanner(System.in);
			Note :-  System.in :- represents Keyboard
3.  Use following methods as per the variable data type. 

		byte		nextByte()
		short		nextShort()
		int		nextInt()
		long		nextLong()
		float		nextFloat()
		double		nextDouble()
		boolean		nextBoolean()
		char		nextCharacter()
		--------------------------------
		String 		next()/nextLine()
these methods are access with object as follows
			objectname.methodname()
------------------------------------------------------------------------------------
Control Statements :- 
		these statements are used to control flow of program execution. 

		there are 3 types 
			1. Conditional
			2. Branching
			3. Looping

Conditional :-  these statements are used to check conditions, based condition true/false
			some statements are executed. here "if" statement is used to check cond. 

Simple If :-
	
	if(condition)
	{
		set of stmts
	}

If-Else:- 
	if(condition)
	{
		set of stmts
	}
	else
	{
		set of stmts
	}
else if :- 
	if(cond-1)
	{
		set of stmts-1
	}
	else if(cond-2)
	{
		set of stmts-2
	}
	........
	........
	else if(cond-n)
	{
		set of stmts-n
	}
	[else
	{
		set of default stmts
	}]
Nested If:- 
	A If condition within another if condition without else part. 
	if(cond-1)
	{
		if(cond-2)
		{
			set of stmts
		}
	}
----------------------------------
Assginments
		1. Accept Month Number (1-12) then print no of days presnet on that month.
			if month nnumber is not b/w 1-12, then show "invalid month".

		2. Accept 3 digit number, then find it's indivisual digits, 
			then check small digit from those digits.
				n = 123
				small digit is 1
				n = 409
				small digit is 0
			Note :- if value not in range of 100-999  then show "Invalid Number".

		3.  Accept a year then check that year is leap year or not.