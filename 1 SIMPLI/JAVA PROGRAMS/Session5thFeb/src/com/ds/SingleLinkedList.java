package com.ds;

public class SingleLinkedList {

	class Node
	{
		int data;
		Node next;  // storing the next node address
		
		Node(int data)
		{
			this.data = data;
			this.next = null;
		}
	}
	
	private Node head = null;
	private Node tail = null;
	
	public void AddNode(int data)
	{
		Node  newNode = new Node(data);
				
		if(head==null)
		{
			head = newNode;
			tail = newNode;
		}
		else
		{
			tail.next = newNode; // update address
			tail = newNode;  // updating value
		}	
	}
	
	
	public void Traverse()
	{
		Node current = head;
		if(head==null)
			System.out.println("List is Empty");
		else
		{
			System.out.println("Values from Linked List");
			while(current!=null)
			{
				System.out.print(current.data + "\t");
				current = current.next;
			}
		}
	}
	
	
	
}
