package com.ds;

import java.util.Scanner;

public class DoubleSearach {

	public static void main(String[] args) {
		DoubleLinkedList  dlist  = new DoubleLinkedList();
		dlist.addNode(100);
		dlist.addNode(200);
		dlist.addNode(300);
		dlist.addNode(400);
		dlist.addNode(500);
		
		dlist.ShowValuesWithForward();

		Scanner sc = new Scanner(System.in);
		System.out.println("\nEnter value to Search");
		int val = sc.nextInt();
		
		if(dlist.searchAnElement(val)==true)
			System.out.println("Value/Node Existed");
		else
			System.out.println("Value/Node not existed");
	}

}
