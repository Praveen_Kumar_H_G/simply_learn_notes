package StackPack;

import java.util.Stack;

public class StackEx1 {

	public static void main(String[] args) {
		Stack<Integer> stk = new Stack<Integer>();
		
		stk.push(101);
		stk.push(102);
		stk.push(103);
		stk.push(104);
		stk.push(105);
		
		System.out.println(stk);
		
		int cnt= stk.size();
		System.out.println(stk.size());
		for(int i=0;i<cnt;i++)
		{
			System.out.println("Removed " + stk.pop());			
		}
		
		if(stk.isEmpty())
			System.out.println("No values are existed in stack");
	}
}
