package StackPack;

public class StackExMain {

	public static void main(String[] args) {
		StackEx stk1 = new StackEx(5);
		stk1.push(10);
		stk1.push(20);
		stk1.push(30);
		stk1.push(40);
		stk1.push(50);
		//stk1.push(60);
		
		System.out.println("Stack Values ");
		stk1.printStack();
		System.out.println();
		int cnt = stk1.size();
		
		for(int i=0;i<cnt;i++)
		{
			System.out.println(stk1.pop());
		}
		
		if(stk1.isEmpty())
			System.out.println("No values in Stack");;
	}

}
