package StackPack;

public class StackEx {

	private int arr[];
	private int top;
	private int capacity;
	
	StackEx(int size)
	{
		arr = new int[size];
		capacity = size;
		top = -1;
	}
	
	public void push(int x)
	{
		if(isFull())
		{
			System.out.println("All values are filled in stack");
			System.exit(0);// stop the process. 
		}
		//else
		//{
			System.out.println("Inserting : " + x);
			arr[++top] = x;
		//}
	}
	
	public boolean isFull()
	{
		if(top==capacity-1)
			return true;
		else
			return false;
	}
	
	public void printStack()
	{
		for(int i=0;i<=top;i++)
		{
			System.out.print(arr[i] + "  " );
		}
	}
	
	public boolean  isEmpty()
	{
		if(top==-1)
			return true;
		else
			return false;
	}
	
	public int pop()
	{
		if(isEmpty())
		{
			System.out.println("Stack is Empty");
			System.exit(0);
			//return 0;
		}
		//else
		return arr[top--];
	}
	
	public int size()
	{
		return top+1;
	}
}
