package QueuePack;

public class QueueEx1 {

	private int arr[];
	private int capacity;
	int front, rear;
	
	QueueEx1(int size)
	{
		capacity = size;
		arr = new int[size];
		rear = -1;
		front = -1;
	}
	
	public void enQueue(int x)
	{
		if(isFull())
		{
			System.out.println("Queue is Full");
			System.exit(0);
		}
		
		if(front==-1)
		{
			front = 0;
		}
		rear++;
		arr[rear] = x;
		System.out.println("Insereted " + x);
	}
	
	public boolean isFull()
	{
		if(front==0  && rear==capacity-1)
			return true;
		else
			return false;
	}
	
	public int deQueue()
	{
		if(isEmpty())
		{
			System.out.println("No values are existed");
			System.exit(0);
		}
		int element = arr[front];
		if(front>=rear)
		{
			front = -1;
			rear = -1;
		}
		else
		{
			front++;
		}
		return element;
	}
	
	public boolean isEmpty()
	{
		if(front==-1)
			return true;
		else
			return false;
	}
	
	public void ShowQueueValues()
	{
		if(isEmpty())
		{
			System.out.println("Queue is Empty");
			System.exit(0);
		}
		for(int i=0;i<=rear;i++)
		{
			System.out.print(arr[i] + "  ");
		}
	}
	
	public int Size()
	{
		return rear+1;
	}
}
