package QueuePack;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueEx {

	public static void main(String[] args) {

		Queue<Integer>  q = new PriorityQueue<Integer>();
		q.add(100);
		q.add(200);
		q.add(300);
		q.add(400);
		q.add(500);
		System.out.println("Queue Values\n" + q);
		
		int n = q.size();
		
		for(int i=0;i<n;i++)
		{
			System.out.println("Removing : " + q.poll());
			System.out.println(q);
		}
		
		if(q.isEmpty())
			System.out.println("No values present in Queue");
	}
}
