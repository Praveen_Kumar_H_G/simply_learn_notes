package ColletionPack;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DeleteItem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		List<String> arrList = new ArrayList<String>();
		arrList.add("Pushpa");
		arrList.add("Yogendra");
		arrList.add("Venugopal");
		arrList.add("Krishna");
		arrList.add("Pushpa");
		
		System.out.println(arrList);
		
	System.out.println("--------------");
	System.out.println("Enter a string to Delete ");
	String str = sc.next();
	
	if(arrList.contains(str))
	{
		arrList.remove(str);
		System.out.println("List After Deletion : " + arrList);
	}
	else
		System.out.println("Not Existed");
	}
}
------------------------------------
package ColletionPack;

import java.util.*;

public class SearchStudentBasedOnRollNo {

	public static void main(String[] args) {
		List<Student>  stdList = new ArrayList<Student>();
		Scanner sc = new Scanner(System.in);
		Student std = new Student(101, "Pavan", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(102, "Kiran", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(103, "Shiva", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(104, "Lava", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(105, "Kalyan", "Java", 12000.00f);
		stdList.add(std);
		
		System.out.println(stdList);
		System.out.println("---------------");
		System.out.println("Roll Number to Search : ");
		int rno = sc.nextInt();
		boolean b = false;
		for(Student s : stdList)
		{
			if(s.getRollno()==rno)
			{
				System.out.println(s);
				b = true;
				break;
			}
		}
		
		if(b==false)
			System.out.println("Student Not Found....");
	}

}
------------------------------------------------searchig by roll
------serchitem----------------
package ColletionPack;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SearchItem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		List<String> arrList = new ArrayList<String>();
		arrList.add("Pushpa");
		arrList.add("Yogendra");
		arrList.add("Venugopal");
		arrList.add("Krishna");
		arrList.add("Pushpa");
		
		System.out.println(arrList);
		
	System.out.println("--------------");
	System.out.println("Enter a string to search ");
	String str = sc.next();
	
	if(arrList.contains(str))
		System.out.println("Existed...");
	else
		System.out.println("Not Existed");
	}

}
---------------------------------------
package ColletionPack;

import java.util.*;

public class StdMain {

	public static void main(String[] args) {
		List<Student>  stdList = new ArrayList<Student>();
		
		Student std = new Student(101, "Pavan", "Java", 12000.00f);
		System.out.println(std.toString());
		stdList.add(std);
		std = new Student(102, "Kiran", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(103, "Shiva", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(104, "Lava", "Java", 12000.00f);
		stdList.add(std);
		std = new Student(105, "Kalyan", "Java", 12000.00f);
		stdList.add(std);
		
		System.out.println(stdList);
		System.out.println("---------------");
		for(Student s : stdList)
		{
			System.out.println(s);
		}
		System.out.println("---------------");
		Iterator itr = stdList.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
------------std itirate---------------
package Sorting;

public class StringSorting {

	public static void main(String[] args) {
		String  str[] = {"Kiran", "Baskar", "Amala", "Devdas","Mohan"};
		
		System.out.println("Actual Strings : ");
		for(int i=0;i<str.length;i++)
		{
			System.out.print(str[i] + "  ");
		}
		
		/*
		 * compareTo() :-  
		 * str1.compareTo(str2)
		 * if first string is bigger than second string it returns >0
		 * if first string is smaller than second string it returns <0
		 * if first string and second string is same it returns 0
		 */
		
		for(int i=0;i<str.length;i++)
		{
			for(int j=i+1;j<str.length;j++)
			{
				if(str[i].compareTo(str[j])>0)
				{
					String temp = str[i];
					str[i] = str[j];
					str[j] = temp;
				}
			}
		}
		
				
		System.out.println("\nSorted Strings in Asending Order : ");
		for(int i=0;i<str.length;i++)
		{
			System.out.print(str[i] + "  ");
		}
	}

}
----------sorting assending--------------------

