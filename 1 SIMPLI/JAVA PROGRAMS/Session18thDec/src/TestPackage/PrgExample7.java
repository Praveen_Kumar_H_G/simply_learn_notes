package TestPackage;
// Example of Assignment Operators 
/**
 * @author Punith HG
 *
 */
public class PrgExample7 {

	public static void main(String[] args) {
		int x = 10;
		System.out.println("X value is : " + x);
		// +=, -=, /=, *=, %=
		//x = x+10;
		x+=10;
		System.out.println("X val After Increment : " + x);
		//x = x-5;
		x-=5;
		System.out.println("X val After Decrement : " + x);
	}
}
