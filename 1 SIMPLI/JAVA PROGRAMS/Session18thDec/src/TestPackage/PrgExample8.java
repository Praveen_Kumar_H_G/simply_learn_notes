package TestPackage;
// Example of Conditional Operators
public class PrgExample8 {

	public static void main(String[] args) {
		int x = 10, y = 5;
		
		System.out.println("X value is : " + x);
		System.out.println("Y value is : " + y);
		
		int big = (x>y) ? x : y;
		
		System.out.println("Big Value is : " + big);
	}
}
