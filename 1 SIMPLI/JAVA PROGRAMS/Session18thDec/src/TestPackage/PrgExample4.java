package TestPackage;
// Example for Arithematic Operators
/**
 * @author Punith HG
 *
 */
public class PrgExample4 {

	public static void main(String[] args) {
		int x = 10, y = 3;
		System.out.println("X value is : " + x);
		System.out.println("Y value is : " + y);
		
		System.out.println("Add : " + (x+y));
		System.out.println("Sub : " + (x-y));
		System.out.println("Mul : " + (x*y));
		System.out.println("Div : " + (x/y));
		System.out.println("Rem : " + (x%y));
	}

}
