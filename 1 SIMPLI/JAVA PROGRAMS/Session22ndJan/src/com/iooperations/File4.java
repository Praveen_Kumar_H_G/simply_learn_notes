package com.iooperations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class File4 {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		//FileInputStream fis = new FileInputStream("C:\\FSDWeekEndBatch\\FilesInfo\\Session22ndJavaWeekend.txt");
		FileReader  fr = new FileReader("C:\\FSDWeekEndBatch\\FilesInfo\\abc.txt");
		BufferedReader  br = new BufferedReader(fr);
		
		String str = br.readLine();
		///System.out.println(str);
	
		while(str!=null)
		{
			System.out.println(str);
			str = br.readLine();
		}
		fr.close();
		br.close();
		
		
	}
}
