package com.apps;
import com.cons.MultiCons;
public class ConsApp3 {

	public static void main(String[] args) {
		MultiCons mc1 = new MultiCons();  // it will call cons without parameters
		mc1.PrintValues();
		
		MultiCons mc2 = new MultiCons(45,67); // it will call cons with parameters
		mc2.PrintValues();
		
	}

}
