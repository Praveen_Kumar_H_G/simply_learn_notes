As a Full Stack Developer, design and develop a backend administrative portal for the Learner’s Academy. Use the GitHub repository to manage the project artifacts. 


Background of the problem statement:

Learner’s Academy is a school that has an online management system. The system keeps track of its classes, subjects, students, and teachers. It has a back-office application with a single administrator login.


The administrator can:

● Set up a master list of all the subjects for all the classes
● Set up a master list of all the teachers
● Set up a master list of all the classes
● Assign classes for subjects from the master list
● Assign teachers to a class for a subject (A teacher can be assigned to different classes for 		different subjects)
● Get a master list of students (Each student must be assigned to a single class)
     

There will be an option to view a Class Report which will show all the information about the class, such as the list of students, subjects, and teachers



Subjects:- 
SubjectID(PK)	SubjectName
11		Englsih
22		Telugu
33		Hindi
44		Maths
55		Science
66		Social
--------------------------
Classes :- 

ClassID(PK)	ClassName
-------------------------
1		Class-I
2		Class-II
3		Class-III
---------------------------
Teachers :- 

TeacherID(PK)	TeacherName	Gender	 Address	Location	Phone	Email	
1001		ABC		
1002		BCA
1003		CBA
1004		ACB
------------------------------------------------------------------------------------
ClassSubjects
------
Sno	ClassID(FK)	SubJectID(FK)
---------------------------------
1	1		11
2	1		33
3	1		44
4	2		11
5	2		33
6	2		44
7	2		55
8	2		66

