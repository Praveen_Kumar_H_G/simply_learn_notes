package com.hibernate.data;
import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class AddStdData {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		System.out.println("Connected....");
		Student std = new Student();
		Scanner sc = new Scanner(System.in);

		System.out.println("Roll Number : ");
		std.setRollno(sc.nextInt());

		System.out.println("Student Name : ");
		std.setSname(sc.next());

		System.out.println("Student Course : ");
		std.setCourse(sc.next());

		System.out.println("Course Fees : ");
		std.setFees(sc.nextFloat());

		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		// save data into database
		s.save(std);  
		t.commit();   

		System.out.println("Row Inserted....");
		

	}

}
