package com.hibernate.data;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class RetriveDataforSpecificRecords {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		System.out.println("Connected....");
		
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Query qry = s.createQuery("from Student");
		qry.setFirstResult(0);
		qry.setMaxResults(2);
		List<Student>  stdall = qry.list();
		System.out.println(stdall);
		
		System.out.println("------------");
		for(Student st:stdall)
		{
			System.out.println(st.getRollno() + "  " + st.getSname() + "  " + st.getCourse() + "  " + st.getFees());
		}
		

	}

}
