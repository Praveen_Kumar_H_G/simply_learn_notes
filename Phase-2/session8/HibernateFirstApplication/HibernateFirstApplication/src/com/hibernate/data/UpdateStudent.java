package com.hibernate.data;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class UpdateStudent {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		System.out.println("Connected....");
		Scanner sc = new Scanner(System.in);
		
		
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Transaction trx = s.beginTransaction(); 
		System.out.println("Enter roll number to modify name of student");
		int rno = sc.nextInt();
		Student st = s.get(Student.class, rno);
		if(st!=null)
		{
			System.out.println("Present Name of the Student : " + st.getSname());
			System.out.println("Enter New name of the student");
			String sna = sc.next();
			st.setSname(sna);
			s.saveOrUpdate(st);  // it wll modify data into db
			
			trx.commit();
			System.out.println("Student Name is Modified");
		}
		else
			System.out.println("Student Not Found");
	}

}
