package com.hibernate.data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class StdApp1 {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		System.out.println("Connected....");
		Student std = new Student();
		std.setRollno(1002);
		std.setSname("Sahasra");
		std.setCourse("Java");
		std.setFees(12000.00f);
		
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		// save data into database
		s.save(std);  
		t.commit();   

		System.out.println("Row Inserted....");
		

	}

}
