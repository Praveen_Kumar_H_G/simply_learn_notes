package com.hibernate.data;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class DeleteStudent {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		System.out.println("Connected....");
		Scanner sc = new Scanner(System.in);
		
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Transaction trx = s.beginTransaction(); 
		System.out.println("Enter roll number to Delete");
		int rno = sc.nextInt();
		Student st = s.get(Student.class, rno);
		if(st!=null)
		{
			System.out.println(st);
			System.out.println("Are u sure(y/n) : ");
			String str = sc.next();
			if(str.equals("y"))
			{
				s.delete(st);
				trx.commit();
				System.out.println("Student Details are Deleted");
			}
		}
		else
			System.out.println("Student Not Found");
	}
}
