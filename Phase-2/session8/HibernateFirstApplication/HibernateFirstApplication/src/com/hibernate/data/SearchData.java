package com.hibernate.data;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SearchData {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		System.out.println("Connected....");
		
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		/*get()  it is from session interface
		 * it will search a row based on primary key column.
			sessionObj.get(<classtype>, argval(primary key))
		 select * from student where rollno=1001
		 */
		Student st = s.get(Student.class, 1001);
		if(st!=null)
			System.out.println(st);
		else
			System.out.println("Student Not Found");
	}
}
