package com.maps.apps;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.maps.entity.Answers;
import com.maps.entity.Questions;
public class QuestionsAdd {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		
		List<Answers>  ansall = new ArrayList<Answers>();
		
		Questions q1 = new Questions();
		q1.setQname("What is a MVC");
		
		Answers ans1 = new Answers();
		ans1.setAnswername("It is a Model View Controller");
		ans1.setPostedBy("Naresh");
		
		ansall.add(ans1);
		
		ans1 = new Answers();
		ans1.setAnswername("It is a new Architecture for Projects");
		ans1.setPostedBy("Mahesh");
		
		ansall.add(ans1);
		
		
		
		q1.setAnswers(ansall);
		
		s.save(q1);
		
		t.commit();
		
		System.out.println("Questions INfo Saved....");
		
		
		
		
		
		
		
		

	}

}
