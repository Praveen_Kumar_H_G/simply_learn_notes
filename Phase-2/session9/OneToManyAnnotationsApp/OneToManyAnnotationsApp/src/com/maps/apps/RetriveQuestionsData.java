package com.maps.apps;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.maps.entity.Answers;
import com.maps.entity.Questions;

public class RetriveQuestionsData {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		
		
		TypedQuery  qry = s.createQuery("from Questions");
		
		List<Questions>  qall = qry.getResultList();
		
		for(Questions q :  qall)
		{
			System.out.println("Question:\n" + q.getQname());
			List<Answers>  a_all = q.getAnswers();
			for(Answers a : a_all)
			{
				System.out.println(a.getAnswername() + " : " + a.getPostedBy());
			}
		}
	}
}
