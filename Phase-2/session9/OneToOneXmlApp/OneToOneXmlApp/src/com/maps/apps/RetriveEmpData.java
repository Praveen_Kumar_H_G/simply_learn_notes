package com.maps.apps;

import java.util.List;

import javax.persistence.TypedQuery;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.maps.entity.Address;
import com.maps.entity.Employee;

public class RetriveEmpData {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		
		TypedQuery  qry = s.createQuery("from Employee");
		List<Employee>  empall = qry.getResultList();

		for(Employee emp : empall)
		{
			System.out.println("Employee ID : " + emp.getEmpid());
			System.out.println("Employee Name : " + emp.getEname());
			System.out.println("Email :"+ emp.getEmail());
			Address adr = emp.getAdrs();
			System.out.println("Door No : " + adr.getDoorno());
			System.out.println("City : " + adr.getCity());
			System.out.println("State : " + adr.getState());
			System.out.println("Pincode : " + adr.getPincode());
			System.out.println("----------------");
		}
	}
}
