package com.maps.apps;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.maps.entity.Address;
import com.maps.entity.Employee;

public class EmpAdd {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		
		Employee emp1 = new Employee();
		emp1.setEname("Pavan Kumar");
		emp1.setEmail("pavan@gmail.com");
		
		Address adrs1 = new Address();
		adrs1.setDoorno("A-125, Saroor Nagar");
		adrs1.setCity("Hyderabad");
		adrs1.setState("TS");
		adrs1.setPincode("500072");
		
		emp1.setAdrs(adrs1);
		adrs1.setEmp(emp1);
		
		s.save(emp1);
		t.commit();
		
		System.out.println("Employee Info Saved....");
		
		
		

	}

}
