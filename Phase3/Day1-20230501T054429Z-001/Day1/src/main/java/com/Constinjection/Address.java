package com.Constinjection;

public class Address {

	private String perAddress;

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(String perAddress) {
		super();
		this.perAddress = perAddress;
	}

	public String getPerAddress() {
		return perAddress;
	}

	public void setPerAddress(String perAddress) {
		this.perAddress = perAddress;
	}

	@Override
	public String toString() {
		return "Address [perAddress=" + perAddress + "]";
	}

}
