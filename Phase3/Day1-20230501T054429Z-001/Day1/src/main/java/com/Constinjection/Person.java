package com.Constinjection;

public class Person {

	private String personId;
	private String name;
	
	private Address address;
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Person(String personId, String name, Address address) {
		super();
		this.personId = personId;
		this.name = name;
		this.address = address;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Person [personId=" + personId + ", name=" + name + ", address=" + address + "]";
	}
		
	
	
}
