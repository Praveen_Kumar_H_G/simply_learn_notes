package com.day4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.day4.dao.StudentDao;
import com.day4.dao.StudentDaoImpl;
import com.day4.model.Student;

/**
 * Hello world!
 *
 */
public class App 
{
	
    public static void main( String[] args )
    {
    	boolean temp=true;
        ApplicationContext context=
        		new ClassPathXmlApplicationContext("config.xml");
        StudentDao studentDao = context.getBean("studentDao",StudentDao.class);
        
//        Student student= new Student(101,"Sakshi goyal","saharanpur");
//        
//        int r = studentDao.insert(student);
//        System.out.println("data inserted"+r);
        
       BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
       
       while(true)
       {
       System.out.println("press 1 to add new student");
       System.out.println("press 2  to display all students");
       System.out.println("press 3 to get single student data");
       System.out.println("press 4 to delete any student");
       System.out.println("press 5 to update any student");
       System.out.println("press 6 to  exit");
       
       try
       {
    	   
    	int input= Integer.parseInt(br.readLine());
    	switch (input) {
		case 1:
			
			System.out.println("enter the id:");
			int id=Integer.parseInt(br.readLine());
			
			System.out.println("enter the name:");
			String name=br.readLine();
			
			System.out.println("enter the city:");
			String city=br.readLine();
			
			Student s= new Student();
			s.setStudentId(id);
			s.setStudentName(name);
			s.setStudentCity(city);
			
			int r = studentDao.insert(s);
			System.out.println("student data inserted"+r);
			System.out.println("*************************************************");
			
			break;
        case 2:
        	List<Student> allStudents = studentDao.getAllStudents();
        	for(Student s1:allStudents)
        	{
        		System.out.println("Id="+s1.getStudentId());
        		System.out.println("Name="+s1.getStudentName());
        		System.out.println("city="+s1.getStudentCity());
        		System.out.println();
        	}
        	System.out.println("*************************************************");
			
        	
			break;
        case 3:
        	System.out.println("enter any id whose data you wanna display!!");
        	int id2=Integer.parseInt(br.readLine());
        	Student student = studentDao.getStudent(id2);
        	System.out.println("Id="+student.getStudentId());
    		System.out.println("Name="+student.getStudentName());
    		System.out.println("city="+student.getStudentCity());
    		System.out.println();
    		System.out.println("*****************************************************");
        	break;
        case 4:
        	System.out.println("enter the Id whose data you wanna delete!!!!!!");
        	int id3=Integer.parseInt(br.readLine());
        	studentDao.deleteStudent(id3);
        	System.out.println("student data of "+id3+" got deleted successfully!!!!!");
        	System.out.println("*****************************************************");
        	break;
        case 5:
        	
        	System.out.println("enter the id whose data you want to update!!!!!");
        	int id4=Integer.parseInt(br.readLine());
        	
        	System.out.println("enter the name:");
			String name1=br.readLine();
			
			System.out.println("enter the city:");
			String city1=br.readLine();
			
			Student s1= new Student();
			s1.setStudentId(id4);
			s1.setStudentName(name1);
			s1.setStudentCity(city1);
        	
			studentDao.updateStudent(s1);
			
			System.out.println("Student data got updated successfully!!!");
        	break;
        case 6:
        	temp=false;
        	
		
		}
    	   
       }
       catch(Exception e)
       {
    	   System.out.println("Invalid input try another one");
    	   System.out.println(e.getMessage());
       }
       }
      
       
       
       
       
       
       
       
        
        
    }
}
