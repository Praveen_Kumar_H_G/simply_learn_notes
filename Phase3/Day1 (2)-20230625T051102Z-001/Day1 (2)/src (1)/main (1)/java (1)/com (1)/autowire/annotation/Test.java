package com.autowire.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	private static Employee emp1;

	public static void main(String[] args)
	{
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("com/autowire/annotation/config4.xml");
		Employee emp1 = context.getBean("emp1",Employee.class);
		System.out.println(emp1);
	}
}
