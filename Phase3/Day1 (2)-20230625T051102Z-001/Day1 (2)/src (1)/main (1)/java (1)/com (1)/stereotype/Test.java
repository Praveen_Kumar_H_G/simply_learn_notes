package com.stereotype;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args)
	{
		ApplicationContext context= 
				new ClassPathXmlApplicationContext("com/stereotype/config5.xml");
	
	Student student=context.getBean("s1",Student.class);
	System.out.println(student);
	}
}
