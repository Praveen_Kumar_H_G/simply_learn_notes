package com.day1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	ApplicationContext context=
    			new ClassPathXmlApplicationContext("config.xml");
    	         Student s1=context.getBean("student1",Student.class);
    	         System.out.println(s1);
    }
}
