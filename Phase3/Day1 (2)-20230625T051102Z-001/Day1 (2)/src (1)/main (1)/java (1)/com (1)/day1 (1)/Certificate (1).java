package com.day1;

public class Certificate {

	private String certiName;

	public Certificate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Certificate(String certiName) {
		super();
		this.certiName = certiName;
	}

	public String getCertiName() {
		return certiName;
	}

	public void setCertiName(String certiName) {
		this.certiName = certiName;
	}

	@Override
	public String toString() {
		return "Certificate [certiName=" + certiName + "]";
	}
	
	
}
