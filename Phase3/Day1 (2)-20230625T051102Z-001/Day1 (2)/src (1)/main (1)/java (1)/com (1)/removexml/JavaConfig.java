package com.removexml;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ComponentScan(basePackages = "com.removexml")
public class JavaConfig {

	
	@Bean
	public Address getAddress()
	{
		return new Address();
	}
	
	@Bean
	public Student getStudent()
	{
		//Student student = new Student(getAddress());
		return new Student(getAddress());
	}
}
