package com.removexml;

import org.springframework.stereotype.Component;


public class Student {
	
	public Address address;
	
	@Override
	public String toString() {
		return "Student [address=" + address + "]";
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Student(Address address) {
		super();
		this.address = address;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void study()
	{
		address.display();
		System.out.println("student is studying");
		
	}

}
