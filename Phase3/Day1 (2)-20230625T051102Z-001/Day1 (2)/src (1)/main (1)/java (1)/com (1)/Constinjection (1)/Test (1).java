package com.Constinjection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		ApplicationContext context= 
				new ClassPathXmlApplicationContext("com/Constinjection/config2.xml");
		Person p1=context.getBean("person1",Person.class);
		System.out.println(p1);
		
	}

}
